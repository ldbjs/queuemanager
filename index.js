// Usage
// -------------------------------------------------------------------------------
// const myManager = new QueueManager({...options})
// let result = await myManager.execute("job key",() => { /* task to run */ }))

export class QueueManager {

	static staticInstanceCounter = 0;
	static staticInstances = {};

	// Static behaviors

	static RegisterInstances(el) {
		el.instanceId = this.staticInstanceCounter++;
		this.staticInstances[el.instanceId] = el;
		return el.instanceId;
	}
	static UnregisterInstances(el) {
		if(typeof this.staticInstances[el.instanceId] !== 'undefined')
			delete this.staticInstances[el.instanceId];
	}
	static GetInstances() {
		return this.staticInstances;
	}
	static GetInstance(id) {
		return this.staticInstances[id];
	}
	static GetStats() {
		let r = {};
		let els = this.GetInstances()
		for(let i in els)
		{
			r[i] = els[i].getStats();
		}
		return r;
	}

	// Instances behaviors

	constructor(options)
	{
		this.instanceId = this.constructor.RegisterInstances(this);

		this.maxJob = parseInt(options.maxJob) || 0; // max running job
		this.delayBeforeRelease = options.delayBeforeRelease || 0; // delay before queue release after the job end
		this.garbageTTL = parseInt(options.garbageLimit) || 30; // nb tick before remove a task from the queue
		this.garbageTick = parseInt(options.garbageLimit) || 1000; // tick duration in ms
		this.name = options.name || this.instanceId; // queue manager name (usefull for debug)

		this.queue = {}; // indexed queue
		this.processingQueue = {}; // indexed queue
		this.waitingQueue = []; // pile queue (FIFO)

		this.stats = {
			label:this.name,
			taskLimit:this.maxJob,
			taskCount:0,
			executionTime: {
				total:0,
				min:null,
				med:null,
				max:null,
			},
			queue: {
				waiting:null,
				processing:null,
			}
		}

		if(this.garbageTTL>0)
			setInterval(() => this.__garbageCollector(), this.garbageTTL);
	}

	getStats() {
		this.stats.queue.waiting = this.waitingQueue.length;
		this.stats.queue.processing = Object.entries(this.processingQueue).length;
		return this.stats;
	}

	__garbageCollector()
	{
		if(this.maxJob>0)
		{
			let nbWaitingQueue = this.waitingQueue.length;
			let nbProcessingJob = Object.entries(this.processingQueue).length;

			for(let key in this.processingQueue)
			{
				if(this.processingQueue[key] > Date.now() - 120*1000)
					delete this.processingQueue[key];
			}
			this.__shiftWaitingQueue();
		}
	}

	async __shiftWaitingQueue()
	{
		if(this.maxJob>0)
		{
			while(this.waitingQueue.length>0 && Object.entries(this.processingQueue).length < this.maxJob)
			{
				let nbWaitingQueue = this.waitingQueue.length;
				let nbProcessingJob = Object.entries(this.processingQueue).length;
				let queueTask = this.waitingQueue.shift()
				queueTask();
			}
		}
	}

	execute(key,task) {
		if(!(this.queue?.[key]))
		{
			this.queue[key] = new QueueTask(this,key,task);
		}
		return this.queue[key].getResult();
	}
}

class QueueTask {
	constructor(queueManager, key, task)
	{
		this.queueManager = queueManager
		this.key = key
		this.task = task

		this.result = new Promise(async (resolve,reject) => {

			let queueStats = this.queueManager.getStats();

			let needToWait = false;
			let nbWaitingQueue = queueStats.queue.waiting;
			let nbProcessingJob = queueStats.queue.processing;

			if(queueStats.taskLimit>0)
			{
				if(nbWaitingQueue > 0 || nbProcessingJob >= queueStats.taskLimit)
					needToWait = true;
			}

			let queueTask = this.__TaskInit.bind(this,resolve);

			if(needToWait)
			{
				this.queueManager.waitingQueue.push(queueTask);
			}
			else
			{
				queueTask();
			}
		});
	}
	getResult()
	{
		return this.result;
	}

	async __TaskInit(resolve)
	{
		this.queueManager.processingQueue[this.key] = Date.now();
		let result = await this.task();
		this.__TaskRelease(resolve, result);
	}
	async __TaskRelease(resolve, result)
	{
		resolve(result);
		delete this.queueManager.processingQueue[this.key];

		this.queueManager.__shiftWaitingQueue();

		if(this.queueManager.delayBeforeRelease>10)
		{
			setTimeout(() => { delete this.queueManager.queue[this.key] }, this.queueManager.delayBeforeRelease);
		}
		else
		{
			delete this.queueManager.queue[this.key];
		}
	}
}

export default QueueManager